// import {AxiosInstance as axios} from "axios";
import axios from 'axios'

export const state = () => ({
  authUser: null,
});

export const mutations = {
  SET_USER(state, data) {
    if (data) {
      state.authUser = data
    } else {
      state.authUser = null
    }
  }
};

export const actions = {
  async login({ commit }, { username, password }) {
    try {
      //ローカル用
      // const res = await axios.post('http://localhost:3001/auth', { "email":username, "pass":password });
      const res = await axios.post('https://f01-express-api.now.sh/auth', { "email":username, "pass":password });

      console.log(res.data.result);
      if ("false"==res.data.result) {
        // if (username !== "demo" || password !== "pass") {
        throw new Error("エラーですよん")
      }
      commit('SET_USER', username)
    } catch (error) {
      throw error
    }
  },
  async logout({ commit }) {
    try {
      commit('SET_USER', null)
    } catch(error) {
      throw error
    }
  }
};

