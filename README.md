# nuxt_login_sample

> Nuxt.js project

## デプロイ

- deployコマンド

npm run deploy

- ログイン画面

https://f01-nuxt-login-sample2.now.sh/

- ソース

https://f01-nuxt-login-sample2.now.sh/_src

## 認証用API

https://gitlab.com/yfujii01/express_api

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

